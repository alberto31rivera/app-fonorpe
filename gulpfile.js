const {src, dest, watch} = require('gulp');  // requiero gulp porque ya lo instalé pasó 2, ver doc principal
const sass = require('gulp-sass') (require('sass')); 

const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');


function css( done ) {
    src('editar/app.scss') // identifico        
        .pipe( sass( {outputStyle: "expanded"} ) )// compilar 

        .pipe( postcss([ autoprefixer() ]) ) // para que soporte navegadores

        .pipe( dest('css') ) // guardar

    done();
}

// función wacth para q revise cambios en css
function dev() {
    watch( 'editar/**/*.scss', css);
    /* watch( 'src/scss/app.scss', css); */
} 

exports.css = css;
exports.dev = dev;