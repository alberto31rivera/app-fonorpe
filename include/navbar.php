<div class="container">
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
            <div class="container-fluid">
              <a class="navbar-brand d-sm-none d-sm-block" href="#"> 
                <img src="img/logo1.jpg" alt="logo" width="100%" height="100%">
              </a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                  
                    <li class="nav-item px-5 text-uppercase border border-1 border-dark mx-1">
                        <a class="nav-link text-dark" href="index.php">Inicio</a>
                    </li>
                    <li class="nav-item px-5 text-uppercase border border-1 border-dark mx-1">
                        <a class="nav-link text-dark" href="nosotros.php">Nosotros</a>
                    </li>
                     
                    

                </ul> 
                <a class="navbar-brand d-none d-sm-none d-md-none d-lg-block" href="index.php" 
                style="padding-right: 2rem !important;
                    padding-left: 3rem !important;
                  "> 
                    <img src="img/logo-2-naturaly.png" alt="Bootstrap" width="100%" height="100%">
                </a>
    
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    
                    <li class="nav-item px-5 text-uppercase border border-1 border-dark mx-1">
                        <a class="nav-link text-dark" href="productos.php">Productos</a>
                    </li>
                     
                    <li class="nav-item px-5 text-uppercase border border-1 border-dark mx-1">
                        <a class="nav-link text-dark" href="contacto.php">Contacto</a>
                    </li> 

                </ul>
    
              </div>
            </div>
        </nav>
    </div>    
