<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appfonorpe</title> 
     

    <link rel="stylesheet" href="css/app.css">
    <!-- ruta con limpiador --> 
    <!-- <link rel="stylesheet" href="optimizado/app.css"> -->

  

</head>
<body>
 
    <?php include 'include/ceja.php';?>

    
    
    <?php include 'include/navbar.php';?>
    

    <div id="carouselExampleCaptions" class="carousel slide pb-5" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>

        <div class="carousel-inner">
          <div class="carousel-item">
            <img src="img/portada-ban3.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption opacity-50 d-none d-md-block">
              <div class="row my-5 bg-primary pb-2"> 
                <h1 class="pt-3 fs-1 text-uppercase text-light pituco">Productos naturales para el mundo</h1>
              </div>
            </div>
          </div>

          <div class="carousel-item active">
            <img src="img/portada-ban2.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption opacity-50 d-none d-md-block">
                <div class="row my-5 bg-primary pb-2"> 
                    <h1 class="pt-3 fs-1 text-uppercase text-light pituco">Productos naturales para el mundo</h1>
                  </div>
            </div>
          </div>
          
          <div class="carousel-item active">
            <img src="img/portada-ban1.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <div class="row my-5 bg-primary pb-2"> 
                    <h1 class="pt-3 fs-1 text-uppercase text-light pituco">Productos naturales para el mundo</h1>
                  </div>
            </div>
          </div>

        </div>
        
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Anterior</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Siguiente</span>
        </button>
    </div>

    <div class="container py-5 pt-5">  
          
        <div class="row pt-5">
            <div class="col-12 text-center px-5">
                
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-bookmark-check" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                    <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                  </svg>

                <h2 class="pituco text-primary pt-5 th2">Qué hacemos</h2>
                <p class="p-5">   
                 Nos dedicamos a la comercialización, producción y distribución a nivel nacional e internacional de productos naturales cumpliendo con máximos estándares de calidad pasando desde el acopio, transporte, almacenaje, manufactura, envasado hasta la distribución.
                </p>
            </div>

            <div class="row py-md-5 py-1 px-5"> 
                
                <div class="col-12 col-md-3 py-3">
                   
                </div>

                <div class="col-6 col-md-2 py-3">
                    <img src="img/confiable.png" class="rounded mx-auto d-block" alt="app-fonorpe">
                    <h3 class="text-center pituco text-dark pt-5 th3">CONFIABLE Y <br> SEGURO</h3>
                </div>
                <div class="col-6 col-md-2 py-3">
                    <img src="img/estandar.png" class="rounded mx-auto d-block" alt="app-fonorpe">
                    <h3 class="text-center pituco text-dark pt-5 th3">ALTOS <br> ESTÁNDARES</h3>
                </div>
                <div class="col-12 col-md-2 py-3">
                    <img src="img/frutos.png" class="rounded mx-auto d-block" alt="app-fonorpe">
                    <h3 class="text-center pituco text-dark pt-5 th3">PRODUCTO <br> DE CALIDAD</h3>
                </div>

                <div class="col-12 col-md-3 py-3">                   
                </div>
                
            </div>


        </div>
    </div>

    
     
    <!-- fin servicios -->
    <div class="container p-5">
        <div class="row p-5 border border-dark rounded-pill">
            <div class="col-12 div col-md-6">
                <h4 class="proxima px-5 pt-5 text-uppercase">Plátano</h4>
                <h4 class="pituco th3 text-justify px-5 pb-3">Banano orgánico de exportación</h4>
                
                <p class="text-justify px-5">Tenemos el mejor banano orgánico para que lo exportes. Contamos con una comunidad de productores que nos ayudan a satisfacer las necesidades de tu empresa o negocio. 
                </p>  
                <p class="px-5 pt-3"> 
                    <button type="button" class="btn btn-primary text-light text-uppercase">
                        <a href="contacto.php" class="text-light">cotizar</a>
                    
                </button>
                </p>
            </div>

            <div class="col-4 div col-md-2 px-2">
                <img class="img-fluid" src="img/banano-1.jpg" alt="app-fonorpe">              
            </div>
            <div class="col-4 div col-md-2 px-2">
                <img class="img-fluid" src="img/banano-2.jpg" alt="app-fonorpe">              
            </div>
            <div class="col-4 div col-md-2 px-2">
                <img class="img-fluid" src="img/banano-3.jpg" alt="app-fonorpe">              
            </div>

        </div>
    </div>
     
    <div class="container-fluid fondo-video" style=" 
    background-image: url('img/masaje-descontracturante-sullana.jpg');
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
 ">
        <div class="row">
            <div class="col-12 text-center text-light" style="padding: 250px 0;">
                <svg xmlns="http://www.w3.org/2000/svg" width="66" height="66" fill="currentColor" class="bi bi-play" viewBox="0 0 16 16">
                    <path d="M10.804 8 5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z"/>
                </svg>

                <h2 class="pituco th2 pt-5 text-light">COMPROMISO</h2>
                <p class="px-5 text-light">En APPFONORPE estamos comprometidos con la comunidad de la región por lo que fomentamos el desarrollo sostenible de nuestro pueblo y de la cultura de nuestros pobladores. <br> Por lo que realizamos charlas y enseñamos los secretos de las frutas y alimentos.</p>
                 
                
            </div>
        </div>
    </div>
 

    <!-- galería -->
    <div class="container-fluid fondo-web py-5 my-5">
        <div class="row pt-5">
            <div class="col-12 pt-5">
                
                <h2 class="text-center pituco text-primary pt-5 th2">GALERÍA</h2>
                                         
                
                <div class="row py-5">
                    <div class="col-12 col-md-2"> 
                    </div> 
                    <div class="col-2 col-md-2"> 
                        <img class="img-fluid pb-5" src="img/galeria1.jpg" alt="app-fonorpe">    
                        <img class="img-fluid pt-3" src="img/galeria2.jpg" alt="app-fonorpe">                                          
                       
                    </div>                      
                     
                    <div class="col-8 col-md-4">            
                        <img class="img-fluid" src="img/galeria-3.jpg" alt="app-fonorpe">            
                        
                    </div>                      
                    <div class="col-2 col-md-2">          
                        <img class="img-fluid pb-5" src="img/galeria4.jpg" alt="app-fonorpe"> 
                        <img class="img-fluid pt-3" src="img/galeria5.jpg" alt="app-fonorpe">                                    
                         
                    </div>
                    <div class="col-12 col-md-2"> 
                    </div>

                </div> 
               

            </div>
        </div>
    </div>
    
    <!-- +2500 -->
    <div class="container py-2">
        <div class="row py-3 pb-5">
            <div class="col-12 col-md-1"></div>
            <div class="col-12 col-md-3 text-center">
                <h2 class="pituco th1">FRUTAS</h2>
            </div>
            <div class="col-12 col-md-7">
                <h2 class="text-center text-md-start pituco thdestacado">Comercialización,  <span class="azul">producción y distribución </span>a nivel nacional e internacional </h2>
            </div>
            <div class="col-12 col-md-1"></div>
        </div>
    </div>

 
   
    <?php include 'include/footer.php';?>

    <!-- boostrap js -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

</body>


</html>