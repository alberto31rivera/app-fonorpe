<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appfonorpe</title> 
     

    <link rel="stylesheet" href="css/app.css">
    <!-- ruta con limpiador --> 
    <!-- <link rel="stylesheet" href="optimizado/app.css"> -->

  

</head>
<body>
 
<?php include 'include/ceja.php';?>

    <?php include 'include/navbar.php';?>
            
    <div class="container">
       <div class="row col-12">
        <img src="img/portada-ban3.jpg" class="d-block w-100" alt="...">
       </div>
    </div>


     
    <div class="container py-5 pt-5">  
          
        <div class="row">
            <div class="col-12 text-center px-5">
                
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-bookmark-check" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                    <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                  </svg>

                <h2 class="pituco text-primary pt-5 th2">Nosotros</h2>
                <p class="p-5">   
                  Razón Social: “Asociación De Pequeños Productores De Frutos Orgánicos Del Norte Del Perú”  - Appfonorpe
                </p>
            </div>

           

        </div>
    </div>

    
     
    <!-- fin servicios -->
    <div class="container p-2">
        <div class="row"> 

            <div class="col-12 div col-md-6 px-2">
                <img class="img-fluid border border-primary" src="img/nosotros.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">              
            </div>
            <div class="col-12 div col-md-6 pb-3">
                <h4 class="proxima px-5 pt-5 text-uppercase">Historia </h4>
                <h4 class="pituco th3 text-justify px-5 pb-3">Comienzo de una historia</h4>
                
                <p class="text-justify px-5">
                HISTORIAL: EL 18/12/2017 se reunieron 29 productores del distrito de Querecotillo y de sus centros poblados para la constitución de una nueva asociación de pequeños productores de frutos orgánicos del norte del Perú. 
               <br><br>
                En febrero del 2018 se registra legalmente ante Sunarp, entidad de registros de empresas en el Perú. Luego en marzo del 2018 se admitieron 201 nuevos productores el cual ayudó a dar inicio a la primera certificación orgánica con la certificadora bio latina, otorgándonos nuestro primer certificado orgánico el día 30/05/2018.
               <br><br>
                Asimismo en el mes de agosto del 2018 iniciamos nuestra certificación de Comercio Justo obteniendo dicho certificado; de Comercio Justo,  el 13 de setiembre del mismo año.
               <br><br>
                En el mes de mayo se inicia la comercialización de banano orgánico con los siguientes importadores “Agronegocio Los Ángeles” y Anpro. Luego en setiembre del 2018 se empezó a exporta banano comercio justo con la marca Dole hasta diciembre del 2021, actualmente se comercializa Sword con diferentes empresas.
                

                </p>  
                 
            </div>
          

        </div>
    </div>
     
    <div class="container-fluid fondo-contacto px-5">
        <div class="row">
            <div class="col-12 text-center text-light" style="padding: 250px 0;">
                <svg xmlns="http://www.w3.org/2000/svg" width="66" height="66" fill="currentColor" class="bi bi-play" viewBox="0 0 16 16">
                    <path d="M10.804 8 5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z"/>
                </svg>

                <h2 class="pituco th2 pt-5 text-light">COMPROMISO</h2>
                <p class="px-5 text-light">En APPFONORPE estamos comprometidos con la comunidad de la región por lo que fomentamos el desarrollo sostenible de nuestro pueblo y de la cultura de nuestros pobladores. <br> Por lo que realizamos charlas y enseñamos los secretos de las frutas y alimentos.</p>
                 
                
            </div>
        </div>
    </div>
 
 
    
    <!-- +2500 -->
    <div class="container py-2 pt-5">
        <div class="row py-3 pb-5">
            <div class="col-12 col-md-1"></div>
            <div class="col-12 col-md-3 text-center">
                <h2 class="pituco th1">FRUTAS</h2>
            </div>
            <div class="col-12 col-md-7">
                <h2 class="text-center text-md-start pituco thdestacado">Comercialización,  <span class="azul">producción y distribución </span>a nivel nacional e internacional </h2>
            </div>
            <div class="col-12 col-md-1"></div>
        </div>
    </div>

     
    <?php include 'include/footer.php';?>     

    

    <!-- boostrap js -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

</body>


</html>