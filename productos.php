<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appfonorpe</title> 
     

    <link rel="stylesheet" href="css/app.css">
    <!-- ruta con limpiador --> 
    <!-- <link rel="stylesheet" href="optimizado/app.css"> -->

  

</head>
<body>
 
<?php include 'include/ceja.php';?>

    <?php include 'include/navbar.php';?>
         

    <div class="container pb-4">
       <div class="row col-12">
        <img src="img/portada-productos.jpg" class="d-block w-100" alt="...">
       </div>
    </div>   

    

    <div class="container py-5 pt-5">  
          
        <div class="row">
            <div class="col-12 text-center px-5">
                
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-bookmark-check" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                    <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                  </svg>

                <h2 class="pituco text-primary pt-5 th2">PRODUCTOS</h2>
                 
            </div>

             
        </div>
    </div>

    <!-- cuadruado --> 
    <div class="container-fluid my-3" style="background-image: url('img/bananas.jpg');background-size: cover;background-repeat: no-repeat;background-position: center;">
        <div class="row">            
            <div class="col-12 text-center text-light" style="padding: 250px 0;"> 
                <div class="container bg-light">
                    <h2 class="pituco bg-light th2 py-2 text-dark">BANANAS</h2> 
                </div>               
            </div>
        </div>
    </div>

    <!-- 2x1 cuadruado -->   
    <div class="container-fluid">
        <div class="row g-2">
            <div class="col-6">
                <div class="p-3 border bg-light py-5" style="background-image: url('img/arandanos.jpg');background-size: auto;background-repeat: no-repeat;background-position: center;min-height:500px;">
                    <h2 class="text-center bg-primary pituco th2 py-2 text-light" style="margin-top:200px!important;">ARANDANOS</h2>
                </div>
            </div>
            <div class="col-6">
                <div class="p-3 border bg-light py-5" style="background-image: url('img/paltas.jpg');background-size: auto;background-repeat: no-repeat;background-position: center;min-height:500px;">
                    <h2 class="text-center bg-primary pituco th2 py-2 text-light" style="margin-top:200px!important;">PALTAS</h2>
                </div> 
            </div>
        </div>
    </div>
     
     

     
    <!-- cuadruado --> 
    <div class="container-fluid my-3" style="background-image: url('img/manzanas.jpg');background-size: cover;background-repeat: no-repeat;background-position: center;">
        <div class="row">            
            <div class="col-12 text-center text-light" style="padding: 250px 0;"> 
                <div class="container bg-light">
                    <h2 class="pituco bg-light th2 py-2 text-dark">MANZANAS</h2> 
                </div>               
            </div>
        </div>
    </div>

    <!-- 2x1 cuadruado -->   
    <div class="container-fluid">
        <div class="row g-2">
            <div class="col-6">
                <div class="p-3 border bg-light py-5" style="background-image: url('img/limones.jpg');background-size: auto;background-repeat: no-repeat;background-position: center;min-height:500px;">
                    <h2 class="text-center bg-primary pituco th2 py-2 text-light" style="margin-top:200px!important;">LIMONES</h2>
                </div>
            </div>
            <div class="col-6">
                <div class="p-3 border bg-light py-5" style="background-image: url('img/naranjas.jpg');background-size: auto;background-repeat: no-repeat;background-position: center;min-height:500px;">
                    <h2 class="text-center bg-primary pituco th2 py-2 text-light" style="margin-top:200px!important;">NARANJAS</h2>
                </div> 
            </div>
        </div>
    </div>


     <!-- cuadruado --> 
     <div class="container-fluid my-3" style="background-image: url('img/uva.jpg');background-size: cover;background-repeat: no-repeat;background-position: center;">
        <div class="row">            
            <div class="col-12 text-center text-light" style="padding: 250px 0;"> 
                <div class="container bg-light">
                    <h2 class="pituco bg-light th2 py-2 text-dark">UVAS</h2> 
                </div>               
            </div>
        </div>
    </div>

    <!-- 2x1 cuadruado -->   
    <div class="container-fluid">
        <div class="row g-2">
            <div class="col-6">
                <div class="p-3 border bg-light py-5" style="background-image: url('img/granadas.jpg');background-size: auto;background-repeat: no-repeat;background-position: center;min-height:500px;">
                    <h2 class="text-center bg-primary pituco th2 py-2 text-light" style="margin-top:200px!important;">GRANADAS</h2>
                </div>
            </div>
            <div class="col-6">
                <div class="p-3 border bg-light py-5" style="background-image: url('img/mandarina.jpg');background-size: auto;background-repeat: no-repeat;background-position: center;min-height:500px;">
                    <h2 class="text-center bg-primary pituco th2 py-2 text-light" style="margin-top:200px!important;">MANDARINAS</h2>
                </div> 
            </div>
        </div>
    </div>

    <!-- galería -->
    <div class="container-fluid fondo-web py-5 my-5">
        <div class="row pt-5">
            <div class="col-12 pt-5">
                
                <h2 class="text-center pituco text-primary pt-5 th2">GALERÍA</h2>
                                         
                
                <div class="row py-5">
                    <div class="col-12 col-md-2"> 
                    </div> 
                    <div class="col-2 col-md-2"> 
                        <img class="img-fluid pb-5" src="img/galeria1.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">    
                        <img class="img-fluid pt-3" src="img/galeria2.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">                                          
                       
                    </div>                      
                     
                    <div class="col-8 col-md-4">            
                        <img class="img-fluid" src="img/galeria-3.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">            
                        
                    </div>                      
                    <div class="col-2 col-md-2">          
                        <img class="img-fluid pb-5" src="img/galeria4.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos."> 
                        <img class="img-fluid pt-3" src="img/galeria5.jpg" alt="Naturaly SPA - Sullana. Servicios que ofrecemos.">                                    
                         
                    </div>
                    <div class="col-12 col-md-2"> 
                    </div>

                </div> 
               

            </div>
        </div>
    </div>
    
    <!-- +2500 -->
    <div class="container py-2">
        <div class="row py-3 pb-5">
            <div class="col-12 col-md-1"></div>
            <div class="col-12 col-md-3 text-center">
                <h2 class="pituco th1">FRUTAS</h2>
            </div>
            <div class="col-12 col-md-7">
                <h2 class="text-center text-md-start pituco thdestacado">Comercialización,  <span class="azul">producción y distribución </span>a nivel nacional e internacional </h2>
            </div>
            <div class="col-12 col-md-1"></div>
        </div>
    </div>

    <?php include 'include/footer.php';?>    

    <!-- boostrap js -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

</body>


</html>